package net.tyan.cloudserver.networking;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.tyan.cloudapi.reference.Operations;
import net.tyan.cloudserver.CloudServer;
import net.tyan.cloudserver.utils.TeamSpeak;

/**
 * Created by Kevin
 */

public class ServerChannelHandler extends SimpleChannelInboundHandler<ByteBuf> {


    @Override
    protected void messageReceived(ChannelHandlerContext ctx, ByteBuf buf) throws Exception {
        int id = buf.readInt();
        Operations task = Operations.getById(id);
        if (task == null) return;
        switch (task) {
            case TEST_CONNECTION:
                ByteBuf byteBuf = Unpooled.buffer();
                byteBuf.writeInt(1);
                ctx.channel().writeAndFlush(byteBuf);
                break;
            case CLOSE:
                System.out.println("Server closed!");
                ctx.close();
                CloudServer.shutdown();
                break;
            case EXIT:
                System.out.println(ctx.channel().remoteAddress() + " disconnected!");
                ctx.close();
                break;
            case RESTART_TEAMSPEAK:
                TeamSpeak.restart();
                break;
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        CloudServer.allChannels.add(ctx.channel());
        System.out.println(ctx.channel().remoteAddress() + " joined the server!");
        super.channelActive(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println(cause.getMessage());
    }
}
